from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.contrib.auth.decorators import login_required


@login_required
def home(request):
    receiptlist = Receipt.objects.filter(purchaser=request.user)
    context = {"allreceipts": receiptlist}
    return render(request, "receipts/home.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def categories(request):
    categorylist = ExpenseCategory.objects.filter(owner=request.user)
    context = {"mycategories": categorylist}
    return render(request, "receipts/my_categories.html", context)


@login_required
def accounts(request):
    accountlist = Account.objects.filter(owner=request.user)
    context = {"myaccounts": accountlist}
    return render(request, "receipts/my_accounts.html", context)


@login_required
def create_expcat(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_expcat.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
